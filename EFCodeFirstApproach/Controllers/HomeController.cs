﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EFCodeFirstApproach.Models;
using PagedList.Mvc;
using PagedList;

namespace EFCodeFirstApproach.Controllers
{
    public class HomeController : Controller
    {
        OperationContext db=new OperationContext();
        
        // GET: Home
        public ActionResult Index(int? page)
        {
            var pageNumber = page ?? 1;
            var pageSize = 10;
            var data=db.Operations.OrderBy(x=>x.ProductId).ToPagedList(pageNumber, pageSize);
            return View(data);
        }
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(Operation s)
        {
            if(ModelState.IsValid==true)
            {
                db.Operations.Add(s);
                int a = db.SaveChanges();
                if (a > 0)
                {
                    // ViewBag.InsertMessage = "<script>alert(Data Inserted...)</script>";
                    // TempData["InsertMessage"] = "<script>alert(Data Inserted...)</script>";
                    TempData["InsertMessage"] = "Data Inserted.......";
                    return RedirectToAction("Index");

                   // ModelState.Clear();
                }
                else
                {
                    ViewBag.InsertMessage = "<script>alert(Data not Inserted...)</script>";
                }
                
            }
            return View();

        }
        public ActionResult Edit(int id)
        {
            var row = db.Operations.Where(model=>model.ProductId == id).FirstOrDefault();
            return View(row);

        }
        [HttpPost]
        public ActionResult Edit(Operation s)
        {
            if(ModelState.IsValid==true)
            {
                db.Entry(s).State = EntityState.Modified;
                int a = db.SaveChanges();
                if (a > 0)
                {
                    TempData["UpdateMessage"] = "Data Updated....";
                    return RedirectToAction("Index");



                }
                else
                {
                    ViewBag.UpdateMessage = "<script>alert(Data not Inserted...)</script>";
                }
            }
          
            return View();

        }
        public ActionResult Delete(int id)
        {
            if(id>0)
            {
                var ProductidRow = db.Operations.Where(model => model.ProductId == id).FirstOrDefault();
                if (ProductidRow != null)
                {
                    db.Entry(ProductidRow).State = EntityState.Deleted;
                    int a=db.SaveChanges();
                    if (a > 0)
                    {
                        TempData["DeleteMessage"]= "<script>alert(Data Deleted...)</script>";
                    }
                    else
                    {
                        TempData["DeleteMessage"] = "<script>alert(Data not Deleted...)</script>";
                    }
                }
            }
            return RedirectToAction("Index");
            
        }
       
    }
}