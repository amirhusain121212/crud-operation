﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace EFCodeFirstApproach.Models
{
    public class OperationContext:DbContext
    {
        public DbSet<Operation> Operations { get; set;}
    }
}